document.addEventListener('click', function (event) {
	if (!event.target.closest('.column')) return;

    const column = event.target.closest('.column');
    window.location = column.getAttribute('data-link');
});

const video1Request = fetch("https://api.osignage.com/static/vara.mp4").then(response => response.blob());
const video1 = document.querySelector('#vid-col-1');
video1Request.then(blob => {
    video1.src = window.URL.createObjectURL(blob);
}); 

const video2Request = fetch("https://api.osignage.com/static/h2o.mp4").then(response => response.blob());
const video2 = document.querySelector('#vid-col-2');
video2Request.then(blob => {
    video2.src = window.URL.createObjectURL(blob);
}); 

const video3Request = fetch("https://api.osignage.com/static/acnee.mp4").then(response => response.blob());
const video3 = document.querySelector('#vid-col-3');
video3Request.then(blob => {
    video3.src = window.URL.createObjectURL(blob);
}); 
